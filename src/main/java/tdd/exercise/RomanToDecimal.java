package tdd.exercise;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class RomanToDecimal {
    public static HashMap<Character, Integer> map = new HashMap<>(){{
        put('I', 1);
        put('V', 5);
        put('X', 10);
        put('L', 50);
        put('C', 100);
        put('D', 500);
        put('M', 1000);
    }};

    public int Convert(String input) {

        int total = 0;

        for(int i = 0; i < input.length()-1; i++) {
            if(map.get(input.charAt(i)) < map.get(input.charAt(i+1))) {
                total -= map.get(input.charAt(i));
            }else {
                total += map.get(input.charAt(i));
            }
        }
        return total+map.get(input.charAt(input.length()-1));
    }
}
