package tdd.exercise;
import static org.junit.Assert.*;
import org.junit.Test;


public class AppTest {
    private FizzBuzz test;


    @Test
    public void testDivisableBy3() {
        test = new FizzBuzz();
        String expected = "Fizz";
        String actual = test.FizzBuzz(27);
        assertEquals(expected, actual);
    }

    @Test
    public void testDivisableBy5() {
        test = new FizzBuzz();
        String expected = "Buzz";
        String actual = test.FizzBuzz(25);
        assertEquals(expected, actual);
    }


    @Test
    public void testDivisableBy3and5() {
        test = new FizzBuzz();
        String expected = "FizzBuzz";
        String actual = test.FizzBuzz(15);
        assertEquals(expected, actual);
    }

    @Test
    public void testOther() {
        test = new FizzBuzz();
        String expected = "7";
        String actual = test.FizzBuzz(7);
        assertEquals(expected, actual);
    }
}
