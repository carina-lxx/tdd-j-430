package tdd.exercise;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    // ""   "1"   "1,2"
    @Test
    public void testEmptyInput() throws Exception {
        Calculator test = new Calculator();
        int expected = 0;
        int actual = test.Add("");
        assertEquals(expected, actual);
    }

    @Test
    public void testOneNumber() throws Exception {
        // "1"
        Calculator test = new Calculator();
        int expected = 7;
        int actual = test.Add("7");
        assertEquals(expected, actual);
    }

    @Test
    public void testTwoNumber() throws Exception {
        // "1,2"
        Calculator test = new Calculator();
        int expected = 3;
        int actual = test.Add("1,2");
        assertEquals(expected, actual);
    }
    @Test
    public void testMultipleNumber() throws Exception {
        // "1\n2,3"
        Calculator test = new Calculator();
        int expected = 6;
        int actual = test.Add("1\n2,3");
        assertEquals(expected, actual);
    }

    @Test
    public void testDelimiter() throws Exception {
        Calculator test = new Calculator();
        int expected = 3;
        int actual = test.Add("//;\n1;2");
        assertEquals(expected, actual);
    }
    @Test
    public void testInputNotNegative() {
        Calculator test = new Calculator();
        assertThrows(Exception.class, () -> {
           throw new Exception("negative not allowed");
        });
    }
}
