package tdd.exercise;
import static org.junit.Assert.*;
import org.junit.Test;

public class RomanToDecimalTest {

    @Test
    public void testTwoLetters() {
        RomanToDecimal test = new RomanToDecimal();
        int expected = 4;
        int actual = test.Convert("IV");
        assertEquals(expected, actual);
    }
    @Test
    public void testFourLetters() {
        RomanToDecimal test = new RomanToDecimal();
        int expected = 2006;
        int actual = test.Convert("MMVI");
        assertEquals(expected, actual);

    }
    @Test
    public void testSixLetters() {
        RomanToDecimal test = new RomanToDecimal();
        int expected = 1944;
        int actual = test.Convert("MCMXLIV");
        assertEquals(expected, actual);

    }
}
